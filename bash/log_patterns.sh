
#!/usr/bin/env bash 

# created by silent-mobius
# purpose: example of function for log writing
# date: 24/01/2022
# version : 0.0.0



# example 1

function log_this(){
    echo "$(date +%Y-%m-%d_%H:%M)--> " "$@"
}

exec 3>&1 1>> some_log_file.log 2>&1

log_this "this is log to special file descriptor" >&3

#---------------------------------------------------





# example 2
function log_print () {
    printf "%s [%s]: %s\n" "$(date '+%Y-%m-%d_%H:%M:%S')" ${FUNCNAME[1]} "$*"
}

exec > >(tee -ia output.log) 2>&1


log_print "this is alog start " >&2


# ------------------------------------------

