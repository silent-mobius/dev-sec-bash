#!/usr/bin/env bash 


while getopts ":a:f:c:h" OPTIONS;
	do
	case $OPTIONS in 
		a) echo "-a was invoked and passed value of $OPTARG"      ;;
		c) echo " -c was invoked and passed value of $OPTARG"      ;;
		f) echo " -f was invoked and passed value of $OPTARG"      ;;	
		h)	echo " usage is -a value -f value -c value -h for help"      ;;
		*)	echo " incorrect option provide"      ;;
	esac
	done
